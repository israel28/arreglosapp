package com.albeip;

import java.util.Scanner;

public class Util {
    static final String LIMPIA = "\033\143";
    static final long ESPERA_DOS_SEGUNDOS = 2 * 1000;

    static void limpiaPantalla() {
        System.out.println(LIMPIA);
    }

    static void espera() throws InterruptedException {
        Thread.sleep(ESPERA_DOS_SEGUNDOS);
    }

    static boolean esSi(String string) {
        if (string==null)
            return false;
        return string.toLowerCase().equals("si");
    }

    static Scanner creaScanner() {
        return new Scanner(System.in);
    }

    static void opcionInvalida() throws InterruptedException {
        System.out.println("Por favor seleccione una opción válida!");
        espera();
    }

    static void adios() throws InterruptedException {
        System.out.println("Hasta la vista baby!!!");
        espera();
        limpiaPantalla();
    }

}
